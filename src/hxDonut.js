/*
hxDonut.js (v0.1.0)

This jQuery utility was developed by Bill Hicks

Dependencies:
    jQuery      (v1.11.3+)
 
MIT License:
    Copyright (c) 2016 Bill Hicks billvanhausen@yahoo.com
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of 
    this software and associated documentation files (the "Software"), to deal in 
    the Software without restriction, including without limitation the rights to 
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all 
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  
*/

/*global jQuery*/

(function ($) {
    "use strict";
    var _defaults = {
        value: 0,
        max: 100
    };
    
    var _math = {
        getX: function(h, a){
            var r = a * (Math.PI/180);
            return ( Math.cos( r ) * 0 ) - ( Math.sin( r ) * h );
            // return Math.sin(90 - a) * (h / Math.sin(90));
        },
        getY: function(h, a){
            var r = a * (Math.PI/180);
            return ( Math.sin( r ) * 0 ) + ( Math.cos( r ) * h );
            // return Math.sin(a) * (h / Math.sin(90));
        }
    };
    
    var _donut = { 
        selector: "",
        data: {},   
        drawBase: function(context){
            
            context.lineWidth = 3;
            context.strokeStyle = "#cccccc";
            context.lineCap = "butt";
                        
            for(var b = 0; b < 360; b += 5){
                context.beginPath();
                context.moveTo(_math.getX(-65, b), _math.getY(-65, b));
                context.lineTo(_math.getX(-80, b), _math.getY(-80, b));
                context.stroke();                
            }
            
        },
        drawHighlight: function(context, percent){
            
            var scale = percent / 100,
                degree = Math.floor(360 * scale);
                
            context.lineWidth = 3;
            context.strokeStyle = "#00aa00";
            context.lineCap = "butt";
            
            // Animated Render
            var v = 0;
            
            var animation = setInterval(function(){
                
                if(v <= degree){
                    context.beginPath();
                    context.moveTo(_math.getX(-55, v), _math.getY(-55, v));
                    context.lineTo(_math.getX(-90, v), _math.getY(-90, v));
                    context.stroke();   

                }else{
                    clearInterval(animation);
                }
                
                v +=5;
                
            }, 15);
            
            // Static Render
            // for(var v = 0; v <= degree; v += 5){
            //     context.beginPath();
            //     context.moveTo(_math.getX(-55, v), _math.getY(-55, v));
            //     context.lineTo(_math.getX(-90, v), _math.getY(-90, v));
            //     context.stroke();                
            // }
        },
        drawText: function(context, percent){
            
            var scale = percent / 100,
                degree = Math.floor(360 * scale);
                
            // Animated Render
            var v = 0,
                d = 0,
                textFill = "#000000",
                arcFill = "#ffffff";
        
            context.font="40px Arial";
            context.textAlign="center";
            
            var animation = setInterval(function(){
                
                if(v <= degree){
                    d = Math.floor((v / 360) * 100);
                    
                    // if(d >= 100){
                    //     arcFill="#00aa00";
                    //     textFill="#ffff00";
                    // }
                    
                    context.fillStyle = arcFill;
                    context.beginPath();
                    context.arc(0,0,55,0,2*Math.PI);
                    context.fill();    
                    
                    
                    context.fillStyle = textFill;
                    context.fillText( d + "%", 0, 15 ); 

                }else{
                    clearInterval(animation);
                }
                
                v +=30;
                
            }, 90);

            
            // Static Render
            // context.fillStyle="#000000";
            // context.font="40px Arial";
            // context.textAlign="center";
            // context.fillText( percent + "%", 0, 15 );
        },
        init: function(element, data, callback){
            
            _donut.selector = element.selector;
            
            _donut.data = $.extend( true, {}, _defaults, data ); 
            
            var canvas = $(_donut.selector + " canvas")[0],
                context = canvas.getContext("2d"); 
            
            var value;
            
            if( _donut.data.max === 0 || _donut.data.value >= _donut.data.max){
                value = 100;
            }else{
                value = Math.floor((_donut.data.value / _donut.data.max) * 100);
            }
            
            context.translate(canvas.width / 2, canvas.height / 2);

            _donut.drawBase(context);
            
            _donut.drawHighlight(context, value);
            
            _donut.drawText(context, value);

            callback();
        }
    };
    
    $.fn.hxDonut = function( data, callback ){
        var $this = this;
         
        _donut.init( $this, data, callback );
        
    };
    
}(jQuery));